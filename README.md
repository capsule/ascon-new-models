# New Models for the Cryptanalysis of ASCON

This is the code of the paper available [on eprint](eprint.iacr.org/2024/298).

Authors:
- Mathieu Degré
- Patrick Derbez
- Lucie Lahaye
- André Schrottenloher

This work has been partially supported by the French
Agence Nationale de la Recherche through the OREO project under Contract
ANR-22-CE39-0015, and through the France 2030 program under grant agreement 
No. ANR-22-PECY-0010.

## SAT Model for Meet-in-the-middle Attacks on Ascon-XOF

The code of the directory `ascon_mitm` corresponds to Section 4 of the paper. 
It requires a functional installation of `Python3` and `PySAT` [see here](https://pysathq.github.io/).
The code has only been tested on a Fedora platform, but may be compatible with
other Linux distributions or OSes.

Contents:

- `util.py` : useful code for exporting the attack path into LateX
- `ascontikz.sty` : LateX style file for MITM attack path figures such
as those found in the paper.
- `sat_util.py` : wrapper around the PySAT solvers, allows to use them more easily
- `ascon_sat.py` : main code file, shows the different results of the paper

To reproduce the results of Section 4 in the paper, run:

    python3 ascon_sat CASE

Where CASE is one of:

- `3rounds` : finding the configuration for a 3-round attack
- `4rounds` : finding the configuration for a 4-round attack
- `5rounds` : proving that there is no 5-round attack
- `4rounds-mem-opt` : proving that there is no better 4-round attack in memory
- `4rounds-time-opt` : proving that there is no better 4-round attack in time

The latter two targets will take some time (up to one or two hours on a desktop
computer).

## MILP Model for Differential Cryptanalysis of the Ascon Permutation

The code of the directory `ascon_milp` corresponds to Section 5 of the paper.
It relies on the [Gurobi MILP solver](https://www.gurobi.com/).


Contents:

- `tune.prm` : parameters for Gurobi
- `src/ascon.cpp` : Ascon permutation model
- `Makefile`

# Installation instructions

Gurobi version 11.0 must be locally installed. The variable `GUROBI_HOME` must be set and point to
the location where `/lib` and `/include` will be found.

Run:

    make

in the directory where `Makefile` is located, in order to compile. This will 
create the executable `ascon` in the same directory.

Run:

    ./ascon nrounds

where nrounds is the number of rounds for the model. This will launch Gurobi, in 
the terminal, which you can stop at any point with CTRL+C. The lower bound will
gradually increase. After up to a few days of computation (on a desktop computer),
the lower bounds reported in the paper will be reached. Note that the output
of the solver will be logged in the file `mip1.log`.



