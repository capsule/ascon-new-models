#!/usr/bin/python3
# -*- coding: utf-8 -*-

#=========================================================================
#Copyright (c) Feb. 2024

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

#=========================================================================



#=========================================================================

# REQUIREMENTS : Python 3 and PySAT (https://pysathq.github.io/)

#=========================================================================

# Author: Lucie Lahaye and André Schrottenloher
# Date: Feb. 2024
# Version: 1

#=========================================================================
"""
Wrapper that simplifies the use of the PySAT package. Requires PySAT. 
CryptominiSAT is optional.
"""

from pysat.card import EncType, CardEnc
from pysat.formula import IDPool
import pysat.solvers

# Global counter for symbols
symbol_counter = 1


class Symbol:

    def __init__(self, chosen_id=None):
        if chosen_id == None:
            global symbol_counter
            self.id = symbol_counter
            symbol_counter += 1
        else:
            self.id = chosen_id

        self.name = None  # debug
        self.negated = False

    def named(name):
        s = Symbol()
        s.name = name
        return s

    def into_raw(self):
        if self.negated:
            return -self.id
        else:
            return self.id

    def __str__(self):
        name = self.name if self.name != None else str(self.id)
        if self.negated:
            return f"~{name}"
        return name

    def __invert__(self):
        new_self = Symbol(self.id)
        new_self.negated = not self.negated
        new_self.name = self.name
        return new_self

    def __and__(self, other):
        raise RuntimeError("conjunction not supported!")

    def __or__(self, other):
        if isinstance(other, Disjunction):
            return other | self
        elif other == False:
            return other
        elif other == True:
            return True
        elif isinstance(other, Symbol):
            return Disjunction([self, other])
        else:
            raise NotImplementedError

    def __xor__(self, other):
        if isinstance(other, Xor):
            return other | symbol
        elif other == False:
            return self
        elif other == True:
            return Xor([self], True)
        elif isinstance(other, Symbol):
            return Xor([self, other])
        else:
            raise NotImplementedError


class Disjunction:

    def __init__(self, clauses):
        self.clauses = clauses

    def into_raw(self):
        return list(map(lambda x: x.into_raw(), self.clauses))

    def __str__(self):
        s = ""
        for i, c in enumerate(self.clauses):
            if i > 0:
                s += " | "
            s += str(c)

        return s

    def __invert__(self):
        raise RuntimeError("negation not supported for a disjunction")

    def __and__(self, other):
        raise RuntimeError("conjunction not supported!")

    def __xor__(self, other):
        raise RuntimeError("disjunction not supported!")

    def __or__(self, other):
        if other == True:
            return True
        elif other == False:
            return False
        elif isinstance(other, Disjunction):
            return Disjunction(self.clauses, other.clauses)
        elif isinstance(other, Symbol):
            return Disjunction(self.clauses + [other])
        else:
            raise NotImplementedError


class Xor:

    def __init__(self, clauses, negated=False):
        self.clauses = clauses
        self.negated = negated

    def into_raw(self):
        return list(map(lambda x: x.into_raw(), self.clauses)), self.negated

    def __str__(self):
        s = ""
        for i, c in enumerate(self.clauses):
            if i > 0:
                s += " ^ "
            if isinstance(c, Disjunction):
                s += "(" + str(c) + ")"
            else:
                s += str(c)

        if self.negated:
            s += " ^ 1"
        return s

    def __invert__(self):
        raise RuntimeError("negation not supported for an xor clause")

    def __and__(self, other):
        raise RuntimeError("conjunction not supported!")

    def __or__(self, other):
        raise RuntimeError("disjunction not supported!")

    def __xor__(self, other):
        if other == True:
            return Xor(self.clauses, not self.negated)
        elif other == False:
            return self
        elif isinstance(other, Xor):
            return Xor(self.clauses + other.clauses,
                       self.negated ^ other.negated)
        elif isinstance(other, Symbol):
            return Xor(self.clauses + [other], self.negated)
        else:
            raise NotImplementedError


def implication(a, b):  # a => b
    return ~a | b


class SolutionWrapper:

    def __init__(self, solution_type, solution):
        self.solution = solution
        self.type = solution_type

    def __getitem__(self, value):
        assert (isinstance(value, Symbol))

        if self.type == "CryptoMiniSAT":
            if value.negated:
                return not self.solution[value.id]
            else:
                return self.solution[value.id]
        elif self.type == "PySAT":
            try:
                assignment = next(
                    filter(lambda v: abs(v) == value.id, self.solution))
            except:
                raise RuntimeError

            assignment = assignment > 0

            if value.negated:
                return not assignment
            return assignment
        else:
            raise NotImplementedError


pysat_pool = IDPool(start_from=10000000)


def cardinality_constraint_le_v1(solver, variables, k):
    """
    Implementation of cardinality constraints from:
    'Sinz, C. (2005, October). Towards an optimal CNF encoding of boolean 
    cardinality constraints. In International conference on principles and practice 
    of constraint programming (pp. 827-831). Berlin, Heidelberg: Springer Berlin Heidelberg.'
    
    This is Section 2, "Encoding Using a Sequential Counter".
    """

    assert (k >= 0)

    # Notations from the paper
    def x(i):
        return variables[i - 1]

    n = len(variables)
    partial_sums = []
    for i in range(n - 1):
        partial_sums.append([])
        for j in range(k):
            partial_sums[-1].append(Symbol())

    def s(i, j):
        return partial_sums[i - 1][j - 1]

    if k > n:
        return

    solver.add(~x(1) | s(1, 1))
    for j in range(2, k + 1):
        solver.add(~s(1, j))
    for i in range(2, n):
        solver.add(~x(i) | s(i, 1))
        solver.add(~s(i - 1, 1) | s(i, 1))
        for j in range(2, k + 1):
            solver.add(~x(i) | ~s(i - 1, j - 1) | s(i, j))
            solver.add(~s(i - 1, j) | s(i, j))
        solver.add(~x(i) | ~s(i - 1, k))
    solver.add(~x(n) | ~s(n - 1, k))


def cardinality_constraint_ge_v1(solver, variables, k):
    cardinality_constraint_le_v1(solver, list(map(lambda v: ~v, variables)),
                                 len(variables) - k)


class Solver:
    """
    Wrapper around a solver from PySAT or CryptominiSAT, if initialized with the 
    command:
    
    s = Solver("CryptoMiniSAT")
    
    in that case it will try to import pycryptosat. Otherwise it will simply use
    pysat.solvers.
    """

    def __init__(self, name, **kwargs):
        if name == "CryptoMiniSAT":
            import pycryptosat
            self.solver = pycryptosat.Solver(**kwargs)
            self.using_pycryptosat = True
        else:
            self.solver = pysat.solvers.Solver(name=name, **kwargs)
            self.using_pycryptosat = False
        self.number_of_clauses = 0

    def add(self, what):
        #print(what)
        if what == False:
            raise RuntimeError
        elif what == True:
            pass
        elif isinstance(what, Symbol):
            self.solver.add_clause([what.into_raw()])
        elif isinstance(what, Disjunction):
            self.solver.add_clause(what.into_raw())
        elif isinstance(what, Xor):
            if not self.using_pycrypto_sat:
                raise RuntimeError(
                    "Xor clauses are only supported with the CryptoMiniSAT solver"
                )
            self.solver.add_xor_clause(*what.into_raw())
        else:
            raise NotImplementedError
        self.number_of_clauses += 1

    def solve(self):
        if self.using_pycryptosat:
            sat, solution = self.solver.solve()

            if sat:
                solution = SolutionWrapper('CryptoMiniSAT', solution)
        else:
            sat = self.solver.solve()
            solution = self.solver.get_model()

            if sat:
                solution = SolutionWrapper('PySAT', solution)

        return sat, solution

    def supports_native_atmost(self):
        if self.using_pycryptosat:
            return False
        return self.solver.supports_atmost()

    def add_atmost(self, variables, k, encoding=None):
        if encoding == None:
            # Native
            if not self.supports_native_atmost():
                raise RuntimeError(
                    "Native atmost constraints aren't supported by this solver"
                )

            self.solver.add_atmost(
                list(map(lambda v: v.into_raw(), variables)), k)
            self.number_of_clauses += 1
        elif encoding == "v1":
            cardinality_constraint_le_v1(self, variables, k)
        else:
            # TEMP
            if self.supports_native_atmost():
                print("Note: this solver supports native 'atmost' constraints")

            # Non-native constraints
            cnf = CardEnc.atmost(lits=list(
                map(lambda v: v.into_raw(), variables)),
                                 bound=k,
                                 encoding=encoding,
                                 vpool=pysat_pool)
            for clause in cnf.clauses:
                self.number_of_clauses += 1
                self.solver.add_clause(clause)

    def add_atleast(self, variables, k, encoding):
        if encoding == "v1":
            cardinality_constraint_ge_v1(self, variables, k)
        else:
            # Native encoding are never supported
            cnf = CardEnc.atleast(lits=list(
                map(lambda v: v.into_raw(), variables)),
                                  bound=k,
                                  encoding=encoding,
                                  vpool=pysat_pool)

            for clause in cnf.clauses:
                self.number_of_clauses += 1
                self.solver.add_clause(clause)
