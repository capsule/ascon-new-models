#!/usr/bin/python3
# -*- coding: utf-8 -*-

#=========================================================================
#Copyright (c) Feb. 2024

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

#=========================================================================



#=========================================================================

# REQUIREMENTS : Python 3 and PySAT (https://pysathq.github.io/)

#=========================================================================

# Author: Lucie Lahaye and André Schrottenloher
# Date: Feb. 2024
# Version: 1

#=========================================================================
"""
This script allows to write the attack path in a LateX file and to compile it.
The compilation requires a custom package built on TikZ (Ascontikz) which is
distributed with this code, and should be in the same directory.

This code should be somewhat cross-platform, but was only tested on Fedora.
"""

import platform
import os

# Sub-directory in which the output files are stored (will be created if it does
# not already exist)
RESULTS = "results"
# Name of the TeX
TEX_STYLE = "ascontikz.sty"

# Header of generated LateX file
STANDALONE_HEADER = """
\\documentclass{standalone}
\\usepackage{amsmath}
\\usepackage{amsfonts}
\\usepackage{amssymb}
\\usepackage{tikz}
\\usepackage{xcolor}
\\usepackage{../ascontikz}
\\colorlet{darkgreen}{green!50!black} \colorlet{darkblue}{blue!50!black}
\\colorlet{darkred}{red!50!black} \colorlet{darkorange}{orange!70!black}
"""


def generate_fig(bit_var_colored, cancelled_bits, duplicate=False):
    """
    Generates the code of the figure according to our custom set of macros.
    """
    state_labels = bit_var_colored.keys()

    # red, blue, green, gray
    res = STANDALONE_HEADER
    res += """\\begin{document}\n"""
    res += """\\begin{figascon}\n"""

    def col(i, j):
        cols = bit_var_colored[s][(i, j)]
        bit_color = ""
        if cols["white"] == 1:
            bit_color = ""
        elif cols["blue"] == 0 and cols["red"] == 0:
            bit_color = "gray"
        elif cols["blue"] == 1 and cols["red"] == 1:
            bit_color = "green"
        elif cols["blue"] == 1:
            bit_color = "blue"
        elif cols["red"] == 1:
            bit_color = "red"
        return bit_color

    for s in state_labels:
        # Bit colors
        for i in range(64 if not duplicate else 32):
            for j in range(5):
                res += """\\drawbit[%s]{%i}{%i}""" % (col(i, j), i, j)
                if duplicate:
                    res += """\\drawbit[%s]{%i}{%i}""" % (col(i, j), i + 32, j)
            res += "\n"

        # Cell borders
        for i in range(64 if not duplicate else 32):
            for j in range(5):
                is_cancelled = cancelled_bits[s][(i, j)]
                if is_cancelled:
                    res += "\\drawborder[%i]{%i}" % (i, j)
                    if duplicate:
                        res += "\\drawborder[%i]{%i}" % (i + 32, j)
            res += "\n"

        res += """\\newround\n\n"""

    res += """\\end{figascon}\n\\end{document}\n"""
    return res


def write_and_compile(code, file_name, open_after=True):
    """
    Write the code to a LateX file, compiles it with pdflateX and opens the file 
    with the system's pdf viewer.
    """
    tex_file_name = file_name.strip(".") + ".tex"
    pdf_file_name = file_name.strip(".") + ".pdf"
    if not os.path.isdir(RESULTS):
        print("Creating directory", RESULTS)
        os.mkdir(RESULTS)
    full_tex_file_name = os.path.join(RESULTS, tex_file_name)
    full_pdf_file_name = os.path.join(RESULTS, pdf_file_name)
    if os.path.isfile(full_tex_file_name):
        input_res = input("Overwrite file? (y/n)")
        if input_res.lower() != "y":
            print("Ending here")
            return
    with open(full_tex_file_name, 'w') as f:
        f.write(code)
    # check that style file is here
    if not os.path.isfile(TEX_STYLE):
        raise Exception("Style file does not exist")
    # now compile with pdflatex, within this directory
    print("Compiling...")

    _platform_system = platform.system()
    if _platform_system == "Linux":
        silence_output = "> /dev/null 2>&1"
    elif _platform_system == "Windows":
        silence_output = "> NUL 2>&1"
    elif _platform_system == "Darwin":
        silence_output = "> /dev/null 2>&1"
    else:
        raise Exception("Unrecognized platform")

    exit_code = os.system("cd " + RESULTS + "; pdflatex " + tex_file_name +
                          " " + silence_output)
    print("Compilation ran with exit code:", exit_code)
    # open file
    if open_after:
        if _platform_system == "Linux":
            exit_code = os.system("xdg-open " + full_pdf_file_name)
        elif _platform_system == "Windows":
            exit_code = os.system("start " + full_pdf_file_name)
        elif _platform_system == "Darwin":
            exit_code = os.system("open " + full_pdf_file_name)
        else:
            raise Exception("Unrecognized platform")
