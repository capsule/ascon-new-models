#!/usr/bin/python3
# -*- coding: utf-8 -*-

#=========================================================================
#Copyright (c) Feb. 2024

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

#=========================================================================



#=========================================================================

# REQUIREMENTS : Python 3 and PySAT (https://pysathq.github.io/)

#=========================================================================

# Author: Lucie Lahaye and André Schrottenloher
# Date: Feb. 2024
# Version: 1

#=========================================================================
"""
Defines and solves the SAT problem to find a configuration for a valid 3- or
4-round preimage attack on Ascon-Hash. Includes the different optimizations
discussed in the paper.

"""

from sat_util import Solver, Symbol, EncType, implication
from util import generate_fig, write_and_compile

# If true, will divide the number of variables by 2
# All of our results consider this case (runtimes without symmetries are much bigger)
WITH_SYMMETRY = True


def search_configuration(rounds_count=4,
                         time_complexity=124,
                         gray=26,
                         file_output_name="test",
                         matching_bit_positions=[],
                         padding_bit=True):
    """
    Searches for a valid attack configuration using our SAT modeling (returns
    the configuration, or unsat).
    
    rounds_count : number of rounds (either 3, 4 or 5)
    blue_degree_of_freedom : degree of freedom of blue in the input message block (determines the
                amount of matching, which is equal, and the complexity of the attack)
    gray : additional number of bits in the input message block which must be gray.
            Determines the memory complexity. Also determines indirectly the amount
            of cancellations.
    file_output_name : name of the LateX file for the output
    matching_bit_positions : positions at which matching bits must occur (allows to
            cut down a SAT problem into several sub-problems, default is [] for no constraints)
    padding_bit : if True, forces the bit number 63 (31 when symmetry is enforced)
            in the input message to be gray, because it's the padding bit.

    """
    if rounds_count not in [3, 4, 5]:
        raise ValueError("Unsupported number of rounds: " + str(rounds_count))

    print("Rounds", rounds_count, "Expected time complexity:", time_complexity,
          "Gray bits", gray)

    # Scaling factor for the number of columns (state size)
    COLUMNS = 32 if WITH_SYMMETRY else 64

    # Scaling factor for the complexities
    _FACTOR = 2 if WITH_SYMMETRY else 1

    #=====================================
    # Definition of Solver and encoding of cardinality constraints
    s = Solver('glucose4')
    card_encoding = EncType.kmtotalizer

    # Color labels
    RED = 'r'
    BLUE = 'b'
    WHITE = 'w'
    colors_list = [RED, BLUE, WHITE]

    # Round labels
    # s0 is the first state, after the first S-Box layer.
    # a1 is the state after the first linear layer. s1 is the state after the second
    # S-Box layer, etc.
    round_labels = []
    for i in range(rounds_count):
        if i == 0:
            round_labels.append(f"s0")
        else:
            round_labels += [f"a{i}", f"s{i}"]

    # Main boolean variables: coloration of bits
    bits_colors = {}
    for round_name in round_labels:
        bits_colors[round_name] = {}
        for color in colors_list:
            bits_colors[round_name][color] = {}
            for i in range(COLUMNS):
                for j in range(5):
                    bits_colors[round_name][color][i, j] = Symbol.named(
                        f"c_{i}_{j}_{color}")

    #=====================================
    # Constraints on the initial state
    s0_payments = [(Symbol.named(f"p1_{i}"), Symbol.named(f"p2_{i}"))
                   for i in range(COLUMNS)]
    for i in range(COLUMNS):
        payment_1, payment_2 = s0_payments[i][0], s0_payments[i][1]

        red_column = [bits_colors['s0'][RED][i, j] for j in range(5)]
        blue_column = [bits_colors['s0'][BLUE][i, j] for j in range(5)]
        white_column = [bits_colors['s0'][WHITE][i, j] for j in range(5)]

        # Padding bit must be set
        # We can even put 2 of them
        if padding_bit:
            if (i + (32 if WITH_SYMMETRY else 0)) in [63]:
                s.add(~blue_column[1])
                s.add(~red_column[1])
            if (i + (32 if WITH_SYMMETRY else 0)) in [62]:
                s.add(~blue_column[1])
                s.add(~red_column[1])

        # if 1 is not red, then no one is red. Same with blue
        # (otherwise trouble with counting the number of gray bits)
        for j in range(5):
            s.add(red_column[1] | ~red_column[j])
            s.add(blue_column[1] | ~blue_column[j])

        # cannot be blue and red at the same time
        s.add(~red_column[1] | ~blue_column[1])

        # if we pay 1, we can cancel the color in bit 3 (set x4 + x3 = 1)
        # blue(1) and not payment_1 => blue(3) i.e. blue(3) or not blue(1) or payment_1
        s.add(payment_1 | ~blue_column[1] | blue_column[3])
        s.add(payment_1 | ~red_column[1] | red_column[3])

        # if we pay 1, we can cancel the color in bit 0 (set x1 = 1) or bit 4 (set x1 = 0)
        s.add(payment_2 | blue_column[0] | ~blue_column[1])
        s.add(payment_2 | blue_column[4] | ~blue_column[1])
        s.add(blue_column[4] | blue_column[0] | ~blue_column[1])
        s.add(payment_2 | red_column[0] | ~red_column[1])
        s.add(payment_2 | red_column[4] | ~red_column[1])
        s.add(red_column[4] | red_column[0] | ~red_column[1])

    #=========================================
    # Propagation of colors through S-Box layers: a1 -> s1, a2 -> s2, ...
    for r in range(1, rounds_count):
        # For each output bit, gives the input bits which appear in its ANF
        sbox_bits = {
            0: [0, 1, 2, 3, 4],
            1: [0, 1, 2, 3, 4],
            2: [1, 2, 3, 4],
            3: [0, 1, 2, 3, 4],
            4: [0, 1, 3, 4]
        }
        # For each output bit, gives the ANDs of input bits which appear in its ANF
        sbox_ands = {
            0: [(4, 1), (1, 4), (2, 1), (1, 2), (1, 0), (0, 1)],
            1: [(3, 2), (2, 3), (3, 1), (1, 3), (2, 1), (1, 2)],
            2: [(4, 3), (3, 4)],
            3: [(4, 0), (0, 4), (3, 0), (0, 3)],
            4: [(4, 1), (1, 4), (1, 0), (0, 1)]
        }

        for i in range(COLUMNS):
            red_before_column = [
                bits_colors[f'a{r}'][RED][i, j] for j in range(5)
            ]
            blue_before_column = [
                bits_colors[f'a{r}'][BLUE][i, j] for j in range(5)
            ]
            white_before_column = [
                bits_colors[f'a{r}'][WHITE][i, j] for j in range(5)
            ]

            red_after_column = [
                bits_colors[f's{r}'][RED][i, j] for j in range(5)
            ]
            blue_after_column = [
                bits_colors[f's{r}'][BLUE][i, j] for j in range(5)
            ]
            white_after_column = [
                bits_colors[f's{r}'][WHITE][i, j] for j in range(5)
            ]

            for j in range(5):
                # If there is a white among the input bits in the ANF, the output is white
                for k in sbox_bits[j]:
                    s.add(
                        implication(white_before_column[k],
                                    white_after_column[j]))

                # If there is a red/blue pair in a product in the ANF, the output is white
                for p in sbox_ands[j]:
                    s.add(white_after_column[j] | ~red_before_column[p[0]]
                          | ~blue_before_column[p[1]])
                    s.add(white_after_column[j] | ~blue_before_column[p[0]]
                          | ~red_before_column[p[1]])

                # If there is some red (resp. blue) in the ANF, the output is red (resp. blue)
                for k in sbox_bits[j]:
                    s.add(
                        implication(red_before_column[k], red_after_column[j]))
                    s.add(
                        implication(blue_before_column[k],
                                    blue_after_column[j]))

    #==================================================

    def prev_bits(k, j):
        """
        A bit after p_L is the XOR of 3 bits in the same line. This function gives
        the positions of these 3 bits depending on the output bit position (k) and 
        line (j).
        """
        if j == 0:
            return k, (k - 19) % COLUMNS, (k - 28) % COLUMNS
        elif j == 1:
            return k, (k - 61) % COLUMNS, (k - 39) % COLUMNS
        elif j == 2:
            return k, (k - 1) % COLUMNS, (k - 6) % COLUMNS
        elif j == 3:
            return k, (k - 10) % COLUMNS, (k - 17) % COLUMNS
        elif j == 4:
            return k, (k - 7) % COLUMNS, (k - 41) % COLUMNS

    #====================================================
    # Propagation of colors through linear layers s0 -> a1, s1 -> a2, ...
    # and definition of the cancellation variables

    color_cancellations = {}
    total_blue_cancelled = []
    total_red_cancelled = []
    cancel_at_round = {}

    for r in range(rounds_count - 1):
        color_cancellations[f"a{r + 1}"] = {RED: {}, BLUE: {}}
        cancel_at_round[f"a{r + 1}"] = []

        for j in range(5):
            for i in range(COLUMNS):
                cancel_red = Symbol.named(f"k_{i}_{j}_s{r}_red")
                cancel_at_round[f"a{r + 1}"].append(cancel_red)

                # No cancellation at these bits in a1
                if r == 0 and j == 2:
                    # bit 2 in a1 is always gray, by construction, so no cancellation
                    # is necessary here
                    s.add(~cancel_red)

                for k in prev_bits(i, j):
                    # If an input bit is white, then the output bit is white
                    s.add(
                        implication(bits_colors[f's{r}'][WHITE][k, j],
                                    bits_colors[f'a{r + 1}'][WHITE][i, j]))
                    # Propagate red unless it is cancelled here
                    s.add(cancel_red
                          | implication(bits_colors[f's{r}'][RED][k, j],
                                        bits_colors[f'a{r + 1}'][RED][i, j]))
                    # Propagate blue (no cancellation)
                    s.add(
                        implication(bits_colors[f's{r}'][BLUE][k, j],
                                    bits_colors[f'a{r + 1}'][BLUE][i, j]))

                color_cancellations[f"a{r + 1}"][RED][i, j] = cancel_red
                total_red_cancelled.append(cancel_red)

    # For the 4-round attack, the cancellations at the final round are counted
    # differently, since it seems the best strategy is to count 2 cancellations by
    # matching bit and allow everything to be green in the state a3.
    if rounds_count == 4:
        for j in range(5):
            for i in range(COLUMNS):
                s.add(~color_cancellations[f"a3"][RED][(i, j)])
    # For the 3-round attack, the only bit we end up cancelling at the final round
    # is bit 1 (it seems more profitable this way)
    elif rounds_count == 3:
        for j in [0, 2, 3, 4]:
            for i in range(COLUMNS):
                s.add(~color_cancellations[f"a2"][RED][(i, j)])

    # Boolean variables that determine whether a bit of the outer part in the
    # final state can be used as a "matching" bit
    if rounds_count == 4:
        # In the 4-round version, it suffices to have a completely non-white column
        # in the state a3 (before the S-Box). That's because we counted the last
        # cancellations differently.
        matching_bits = []
        for i in range(COLUMNS):
            good_bit = Symbol.named(f"good_{i}")
            matching_bits.append(good_bit)
            for j in [0, 1, 2, 3, 4]:
                # bit white => not good
                s.add(~bits_colors[f"a3"][WHITE][(i, j)] | ~good_bit)
    else:
        # In the 3-round and 5-round version, we simply count the non-white bits in final state
        matching_bits = [
            ~bits_colors[f"s{rounds_count - 1}"][WHITE][(i, 0)]
            for i in range(COLUMNS)
        ]

    # additional constraint to place some of the matching bits
    for b in matching_bit_positions:
        s.add(matching_bits[b])

    #==============================================================

    constraints = []
    for payments in s0_payments:
        constraints.append(payments[0])
        constraints.append(payments[1])

    def negate(bits):
        return list(map(lambda v: ~v, bits))

    # The three arrays
    # matching_bits, red_message_bits, blue_message_bits, will determine the
    # complexity of the attack (together with cancellations)
    red_message_bits = [bits_colors["s0"][RED][(i, 1)] for i in range(COLUMNS)]
    blue_message_bits = [
        bits_colors["s0"][BLUE][(i, 1)] for i in range(COLUMNS)
    ]

    # This variable counts the additional cancellations at the last round in the
    # 4-round case
    degree_of_matching = 128 - time_complexity
    final_cancels = (2 * degree_of_matching if rounds_count == 4 else 0)
    # We use less constraints than we could, so that the term does not dominate the
    # attack complexity
    less_constraints = 2
    # This is the number of outer loops in the attack (due to 1 bit of padding)
    loops = 65

    #========================
    # Cardinality constraints

    # There shouldn't be too many constraints on the outer part: N <= T - 65
    s.add_atmost(constraints,
                 int((time_complexity - loops) // _FACTOR) - less_constraints,
                 card_encoding)

    # constraint corresponding to the merged list
    s.add_atmost(negate(matching_bits), (time_complexity - 64) // _FACTOR,
                 card_encoding)

    # constraint corresponding to the red list
    s.add_atmost(negate(blue_message_bits), (time_complexity - 64) // _FACTOR,
                 card_encoding)

    # constraint corresponding to the blue list
    # /!\ we need to subtract the amount of "final" cancellations which aren't counted
    # in the 4-round case
    s.add_atmost(
        negate(red_message_bits) + total_red_cancelled,
        (time_complexity - 64 - final_cancels) // _FACTOR, card_encoding)

    # constraint on amount of gray (determines the memory)
    s.add_atmost(blue_message_bits + red_message_bits, (64 - gray) // _FACTOR,
                 card_encoding)

    #============================================
    # Running the solver and results

    print(" - Number of clauses:", s.number_of_clauses)
    _t1 = time.time()
    sat, solution = s.solve()
    _t2 = time.time()

    if sat:
        for round_name in bits_colors.keys():
            for color in bits_colors[round_name].keys():
                for coords in bits_colors[round_name][color]:
                    bits_colors[round_name][color][coords] = \
                        solution[bits_colors[round_name][color][coords]]

        for round_name in color_cancellations.keys():
            for i in range(COLUMNS):
                for j in range(5):
                    if (i, j) in color_cancellations[round_name][RED]:
                        color_cancellations[round_name][RED][i, j] = \
                            solution[color_cancellations[round_name][RED][i, j]]
                    else:
                        color_cancellations[round_name][RED][i, j] = False
                    color_cancellations[round_name][BLUE][i, j] = False

        matching_bits = list(map(lambda v: solution[v], matching_bits))

        if rounds_count == 4:
            for i in range(COLUMNS):
                # color matching bits in green
                if matching_bits[i]:
                    bits_colors[round_labels[-1]][WHITE][i, 0] = False
                    bits_colors[round_labels[-1]][BLUE][i, 0] = True
                    bits_colors[round_labels[-1]][RED][i, 0] = True

        print("Statistics:")
        red_message_bits = list(map(lambda v: solution[v], red_message_bits))
        blue_message_bits = list(map(lambda v: solution[v], blue_message_bits))
        print(" - Red bits:", sum(red_message_bits) * _FACTOR)
        print(" - Blue bits:", sum(blue_message_bits) * _FACTOR)
        print(" - Degrees of matching:", sum(matching_bits) * _FACTOR)
        total_blue_cancelled = list(
            map(lambda v: solution[v], total_blue_cancelled))
        total_red_cancelled = list(
            map(lambda v: solution[v], total_red_cancelled))
        print(" - Blue cancellations:", sum(total_blue_cancelled) * _FACTOR)
        print(" - Red cancellations:",
              sum(total_red_cancelled) * _FACTOR + final_cancels)

        #=====================================
        # Recompute the complexity of the attack

        memory_complexity = _FACTOR * min(
            sum(red_message_bits) + sum(total_blue_cancelled),
            sum(blue_message_bits) + sum(total_red_cancelled) +
            final_cancels // _FACTOR)
        print(" - Memory complexity:", memory_complexity)
        # merging_complexity = max(dof_red + total_blue_cancelled, dof_blue + total_red_cancelled, dof_red + dof_blue - dom)
        blue_list_size = _FACTOR * (sum(blue_message_bits) +
                                    sum(total_red_cancelled)) + final_cancels
        red_list_size = _FACTOR * (sum(red_message_bits) +
                                   sum(total_blue_cancelled))
        merged_list_size = _FACTOR * (sum(red_message_bits) +
                                      sum(blue_message_bits) -
                                      sum(matching_bits))
        merging_complexity = max(blue_list_size, red_list_size,
                                 merged_list_size)
        print(
            " - Merging complexity:", merging_complexity,
            f"(= max({blue_list_size}, {red_list_size}, {merged_list_size}))")
        # time_complexity = max(loops + total_constraints_first_state, loops + merging_complexity + repetitions)
        repetitions = 64 - 1 - _FACTOR * (sum(red_message_bits) +
                                          sum(blue_message_bits))
        time_complexity_1 = loops + _FACTOR * sum(
            list(map(lambda v: solution[v], constraints)))
        time_complexity_2 = loops + merging_complexity + repetitions
        time_complexity = max(time_complexity_1, time_complexity_2)
        print(" - Recomputed time complexity:", time_complexity,
              f"(= max({time_complexity_1}, {time_complexity_2}))")

        print("Parameters:")
        if WITH_SYMMETRY:
            print("(Duplicate the pattern)")

        red_bits = [i for i in range(COLUMNS) if red_message_bits[i]]
        blue_bits = [i for i in range(COLUMNS) if blue_message_bits[i]]
        print(" - Red bits:", red_bits)
        print(" - Blue bits:", blue_bits)

        matching_points = [i for i in range(COLUMNS) if matching_bits[i]]
        print(" - Matching bits:", matching_points)

        constraints = []
        for i in range(COLUMNS):
            payment_1, payment_2 = s0_payments[i]
            payment_1 = solution[payment_1]
            payment_2 = solution[payment_2]

            if payment_1:
                constraints.append(f"A({i}, 3) ^ A({i}, 4)")

            if payment_2:
                is_b0_red = bits_colors['s0'][RED][i, 0]
                is_b0_blue = bits_colors['s0'][BLUE][i, 0]
                is_b0_gray = not (is_b0_red or is_b0_blue)

                if is_b0_gray:
                    constraints.append(f"A({i}, 1)")
                else:
                    constraints.append(f"~A({i}, 1)")
        print(" - Constraints:", constraints)

        cancellations = []
        for r in range(rounds_count - 1):
            for i in range(COLUMNS):
                for j in range(5):
                    is_blue_cancelled = color_cancellations[f"a{r + 1}"][BLUE][
                        i, j]
                    is_red_cancelled = color_cancellations[f"a{r + 1}"][RED][i,
                                                                             j]

                    if is_blue_cancelled:
                        cancellations.append((f"a{r + 1}", 'b', i, j))
                    if is_red_cancelled:
                        cancellations.append((f"a{r + 1}", 'r', i, j))
        print(" - Bit cancellations:", cancellations)

        #=========================================
        # Display the diffusion pattern in the terminal

        print("Diffusion pattern:")
        COLORS = {
            "HEADER": "\033[95m",
            "BLUE": "\033[104m",
            "GREEN": "\033[102m",
            "RED": "\033[101m",
            "WHITE": "\033[107m",
            "GRAY": "\033[100m",
            "ENDC": "\033[0m",
        }

        r = f"{COLORS['RED']} {COLORS['ENDC']}"
        b = f"{COLORS['BLUE']} {COLORS['ENDC']}"
        v = f"{COLORS['GREEN']} {COLORS['ENDC']}"
        w = f"{COLORS['WHITE']} {COLORS['ENDC']}"
        g = f"{COLORS['GRAY']} {COLORS['ENDC']}"

        def print_diffusion_pattern(round_name, colors):
            print(f'--- {round_name}:')
            for j in range(5):
                for i in range(COLUMNS):
                    if colors[WHITE][i, j]:
                        color = w
                    else:
                        if colors[RED][i, j] and not colors[BLUE][i, j]:
                            color = r
                        elif colors[BLUE][i, j] and not colors[RED][i, j]:
                            color = b
                        elif colors[BLUE][i, j] and colors[RED][i, j]:
                            color = v
                        else:
                            color = g

                    print(color, end='')
                print('')

        for round_name in round_labels:
            colors = bits_colors[round_name]
            print_diffusion_pattern(round_name, colors)

        print("Solving time:", round(_t2 - _t1), "seconds")
        #=========================================
        # Output the diffusion pattern to a lateX file and compile it

        bit_var_colored = {}

        # add the missing first round
        bit_var_colored["a0"] = {}
        for i in range(COLUMNS):
            bit_var_colored["a0"][(i, 0)] = {
                "white": bits_colors["s0"][WHITE][i, 1],
                "red": bits_colors["s0"][RED][i, 1],
                "blue": bits_colors["s0"][BLUE][i, 1],
            }
            for j in range(1, 5):
                bit_var_colored["a0"][(i, j)] = {
                    "white": False,
                    "red": False,
                    "blue": False
                }

        for round_name in round_labels:
            bit_var_colored[round_name] = {}
            for i in range(COLUMNS):
                for j in range(5):
                    bit_var_colored[round_name][(i, j)] = {
                        "white": bits_colors[round_name][WHITE][i, j],
                        "red": bits_colors[round_name][RED][i, j],
                        "blue": bits_colors[round_name][BLUE][i, j],
                    }
                # correct last round
                if round_name == round_labels[-1]:
                    for j in range(1, 5):
                        bit_var_colored[round_name][(i, j)] = {
                            "white": True,
                            "red": False,
                            "blue": False
                        }

        cancelled_bits = {}
        for round_name in round_labels:
            cancelled_bits[round_name] = {}
            for i in range(COLUMNS):
                for j in range(5):
                    if round_name.startswith('a'):
                        is_blue_cancelled = color_cancellations[round_name][
                            BLUE][i, j]
                        is_red_cancelled = color_cancellations[round_name][
                            RED][i, j]
                    else:
                        is_blue_cancelled = False
                        is_red_cancelled = False

                    cancelled_bits[round_name][(
                        i, j)] = is_red_cancelled or is_blue_cancelled
        cancelled_bits["a0"] = {}
        for i in range(COLUMNS):
            for j in range(5):
                cancelled_bits["a0"][(i, j)] = False

        #print( cancelled_bits )
        fig = generate_fig(bit_var_colored,
                           cancelled_bits,
                           duplicate=WITH_SYMMETRY)
        write_and_compile(fig, file_output_name)

    else:
        print("unsat")
        print("Solving time:", round(_t2 - _t1), "seconds")


_HELP = """
Usage:

python3 ascon_sat CASE

Where CASE is one of:
- '3rounds' : finding the configuration for a 3-round attack
- '4rounds' : finding the configuration for a 4-round attack
- '5rounds' : proving that there is no 5-round attack
- '4rounds-mem-opt' : proving that there is no better 4-round attack in memory
- '4rounds-time-opt' : proving that there is no better 4-round attack in time

"""

if __name__ == "__main__":

    import sys
    import time

    if len(sys.argv) < 2:
        print(_HELP)
        sys.exit(0)
    else:
        case = sys.argv[1]
        if case not in [
                '3rounds', '4rounds', '5rounds', '4rounds-mem-opt',
                '4rounds-time-opt'
        ]:
            print(_HELP)
            sys.exit(0)

        elif case == '3rounds':
            # a couple of minutes (tiny help by setting matching bits by hand)
            search_configuration(rounds_count=3,
                                 time_complexity=114,
                                 gray=26,
                                 file_output_name="ascon_3rounds",
                                 matching_bit_positions=[2, 3, 6, 12])

        elif case == '4rounds':
            # around 400 seconds
            search_configuration(rounds_count=4,
                                 time_complexity=124,
                                 gray=26,
                                 file_output_name="ascon_4rounds",
                                 matching_bit_positions=[28])

        elif case == '4rounds-mem-opt':
            # remove the padding bit constraint and force a matching bit to be at position 0 (by symmetry)
            search_configuration(rounds_count=4,
                                 time_complexity=124,
                                 gray=28,
                                 file_output_name="ascon_4rounds",
                                 padding_bit=False,
                                 matching_bit_positions=[0])

        elif case == '5rounds':
            # no constraints on gray bits, no padding (makes the problem easier),
            # force a matching bit to be at position 0 (by symmetry)
            # makes the proof of unsat rather fast (around 1 minute)
            search_configuration(rounds_count=5,
                                 time_complexity=127,
                                 gray=0,
                                 padding_bit=False,
                                 matching_bit_positions=[0])

        elif case == '4rounds-time-opt':
            # no constraints on gray bits, no padding (makes the problem easier),
            # force a matching bit to be at position 0 (by symmetry)
            # each subproblem takes a couple of minutes
            for i in range(1, 32):
                print("Placing matching_bit at position", i)
                search_configuration(rounds_count=4,
                                     time_complexity=122,
                                     gray=0,
                                     padding_bit=False,
                                     matching_bit_positions=[0, i])
