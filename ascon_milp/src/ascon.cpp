#include <set>
#include <cmath>
#include <fstream>
#include <map>

#include "gurobi_c++.h"

using namespace std;


/* Modeling of Ascon S-Boxes for the first round. */
void ascon_Sbox_first(GRBModel & model, vector<GRBVar> & x, vector<GRBVar> & y, 
                      vector<GRBVar> & ca, vector<GRBVar> & pr0, 
                      vector<GRBVar> & pr1, GRBLinExpr & obj) {
  for (unsigned i = 0; i < 64; ++i) {
    auto & d = ca[i];
    model.addConstr(x[i] == 0);
    model.addConstr(x[i + 64] == 0);
    model.addConstr(x[i + 128] == 0);
    model.addConstr(x[i + 192] == 0);
    model.addConstr(x[i + 256] == 0);
    auto & y0 = y[i];
    auto & y1 = y[i + 64];
    auto & y2 = y[i + 128];
    auto & y3 = y[i + 192];
    auto & y4 = y[i + 256];

    model.addConstr(y0 + y1 + y2 + y3 + y4 <= 5*d);
    model.addConstr(y0 + y1 + y2 + y3 + y4 >= d);

    auto & p0 = pr0[i];
    auto & p1 = pr1[i];

    model.addConstr(p0 == 0);

    model.addConstr((1-y4) + (1-y2) + y0 + p1 >= 1 ).set(GRB_IntAttr_Lazy, 1);
    model.addConstr((1-y4) + (1-y3) + y1 + y0 + p1 >= 1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(y4 + y3 + (1-y2) + (1-y0) + p1 >= 1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr((1-y4) + (1-y3) + (1-y1) + (1-y0) + p1 >= 1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(y4 + (1-y3) + y2 + y1 + (1-y0) + p1 >= 1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(y3 + (1-y2) + (1-y1) + (1-y0) + p1 >= 1).set(GRB_IntAttr_Lazy, 1);

    obj += p1 + 2*d;
  }
}

/* Modeling of Ascon S-Boxes for the last round */
void ascon_Sbox_last(unsigned r, GRBModel & model, vector<GRBVar> & x, vector<GRBVar> & y, 
                    vector<GRBVar> & ca, vector<GRBVar> & pr0, 
                    vector<GRBVar> & pr1, GRBLinExpr & obj) {
  for (unsigned i = 0; i < 64; ++i) {
    auto & d = ca[64*r + i];
    model.addConstr(y[320*r + i] == 0);
    model.addConstr(y[320*r + i + 64] == 0);
    model.addConstr(y[320*r + i + 128] == 0);
    model.addConstr(y[320*r + i + 192] == 0);
    model.addConstr(y[320*r + i + 256] == 0);
    auto & x0 = x[320*r + i];
    auto & x1 = x[320*r + i + 64];
    auto & x2 = x[320*r + i + 128];
    auto & x3 = x[320*r + i + 192];
    auto & x4 = x[320*r + i + 256];

    model.addConstr(x0 + x1 + x2 + x3 + x4 <= 5*d);
    model.addConstr(x0 + x1 + x2 + x3 + x4 >= d);

    auto & p0 = pr0[64*r + i];
    auto & p1 = pr1[64*r + i];

    model.addConstr(p0 + p1 >= d);

    model.addConstr(x4 + (1-x3) + p1 >= 1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(x2 + (1-x1) + p1 >= 1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr((1-x4) + x0 + p1 >= 1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr((1-x2) + (1-x0) + p1 >= 1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr((1-x3) + x2 + (1-x1) + p0 >= 1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(x4 + (1-x3) + (1-x0) + p0 >= 1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(x4 + (1-x3) + (1-x2) + x1 + p0 >= 1 ).set(GRB_IntAttr_Lazy, 1);
    model.addConstr((1-x4) + x3 + (1-x1) + x0 + p0 >= 1 ).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(x4 + x2 + (1-x1) + (1-x0) + p0 >= 1).set(GRB_IntAttr_Lazy, 1);

    obj += 2*p1 + p0 + d;
  }
}


/* Modeling of Ascon S-Boxes */
void ascon_Sbox_lazy(unsigned r, GRBModel & model, vector<GRBVar> & x, vector<GRBVar> & y, 
                    vector<GRBVar> & ca, vector<GRBVar> & pr0, 
                    vector<GRBVar> & pr1, GRBLinExpr & obj) {
  for (unsigned i = 0; i < 64; ++i) {
    auto & d = ca[64*r + i];
    auto & x0 = x[320*r + i];
    auto & x1 = x[320*r + i + 64];
    auto & x2 = x[320*r + i + 128];
    auto & x3 = x[320*r + i + 192];
    auto & x4 = x[320*r + i + 256];
    auto & y0 = y[320*r + i];
    auto & y1 = y[320*r + i + 64];
    auto & y2 = y[320*r + i + 128];
    auto & y3 = y[320*r + i + 192];
    auto & y4 = y[320*r + i + 256];

    // Input Hamming weight
    GRBLinExpr hwx = x0 + x1 + x2 + x3 + x4;
    // Output Hamming weight
    GRBLinExpr hwy = y0 + y1 + y2 + y3 + y4;

    model.addConstr(hwx >= d);
    model.addConstr(hwy >= d);

    model.addConstr(hwx + hwy >= 3*d);
    model.addConstr(2*hwx + hwy <= 13*d);

    auto & p0 = pr0[64*r + i];
    auto & p1 = pr1[64*r + i];

    model.addConstr(2*hwx + hwy <= 8 + 5*p0);
    model.addConstr(hwx + hwy <= 8 + p1);

    obj += 2*d + p0 + p1;

    model.addConstr(p0 >= p1);

    // constraints 8
    model.addConstr(x4 + (1-x3) + p0 >= d).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(x2 + x0 + p0 >= d).set(GRB_IntAttr_Lazy, 1);
    model.addConstr((1-x2) + (1-x0) + p0 >= d).set(GRB_IntAttr_Lazy, 1);
    model.addConstr((1-x4) + x0 + p0 >= d).set(GRB_IntAttr_Lazy, 1);
    model.addConstr((1-x1) + (1-x0) + p0 >= d).set(GRB_IntAttr_Lazy, 1);

    // constraints 4
    model.addConstr((1-x3) + x2 + (1-x1) + p1 >= 1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(x4 + (1-x3) + (1-x0) + p1 >= 1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(x4 + (1-x3) + (1-x2) + x1 + p1 >= 1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr((1-x4) + x3 + (1-x1) + x0 + p1 >= 1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(x4 + x2 + (1-x1) + (1-x0) + p1 >= 1).set(GRB_IntAttr_Lazy, 1);


    // Lazy constraints for valid transitions
    string s = "x0 x1 x2' x3' y2'  + x0' x1' x4' y1' y2'  + x0 x3' x4 y0' y1' y2  + x0 x1 x3' x4 y1 y2'  + x0 x1' x4 y0 y1' y2'  + x0 x1' x2' x3' x4 y3'  + x1 x3' y0' y2 y3' y4 + x1 x3' y0 y2 y3' y4' + x0 x1' x2 x3 x4 y0' y4 + x0' x1 x2 y0' y2' y3 y4 + x0' x1 x2 y0 y2' y3' y4 + x0' x1 x2 y0 y2' y3 y4' + x0' x1 x2 y0' y2' y3' y4' + x0' x1' x3 y0' y4' + x0' x1' x2 x3 x4 y1'  + x0' x1' x2' x3' x4 y1'  + x0 x1 x2 x3' y0' y2  + x0' x3' x4' y0' y1 y2  + x0 x1 x2' x4 y1' y2  + x0' x1 x4' y0 y1' y2  + x0 x1 x2 x3 x4' y2'  + x0' x1 x4' y0 y1 y2'  + x0 x3' x4 y0' y1 y2'  + x0' x3' x4' y0' y1' y2'  + x0' x1' x2' x3' x4' y3'  + x0 x1 x2' x3 y2 y3'  + x0 x1 x2 x3' y2 y3'  + x1 x2' x3 x4 y0' y4 + x0' x2 x3 x4' y0' y4 + x1' x2' x3 x4' y0' y4 + x0' x1 x3 y0 y1' y4 + x0 x1' x3 y0 y1' y4 + x0 x1' x3 y0 y2' y4 + x0' x1' x2' x3 x4 y4' + x0 x1 x2' x3 x4' y4' + x0 x1' x3 x4 y0 y4' + x0' x2 x3 x4' y0 y4' + x0 x1 x2' x3 y0' y4' + x0 x2 x3 x4' y0' y4' + x0 x1 x2 x3' y2 y4' + x1 x3 x4' y0 y3 y4' + x1 x3' y0' y2 y3 y4' + x1 x2' y0' y2' y3 y4' + x0' x3 y0' y2 y3' y4' + x0 x2' x3 x4 y0 y1 y2'  + x0 x1' x2' x3' x4' y0' y3  + x0' x1' x3' x4 y0' y1 y3  + x0 x1' x2' x3' x4' y1' y3  + x0 x1' x3' x4' y0' y1' y3  + x0' x1' x3' x4 y1 y2' y3  + x0' x1' x2 x3' x4 y1 y3'  + x0' x1' x2 x3' y1 y2 y3'  + x0 x1' x4 y0 y1 y2 y3'  + x0' x2' x3 x4 y0' y1 y4 + x0 x3 x4' y0 y1' y2 y4 + x0' x3 x4 y0 y1 y2' y4 + x0' x1 x3 x4 y0' y3 y4 + x1 x3 x4' y0' y2 y3 y4 + x1 x3' y0 y1' y2 y3 y4 + x0' x1 x2' y0 y2' y3 y4 + x0' x3 x4 y0 y1 y3' y4 + x3 x4' y0 y1 y2 y3' y4 + x0' x1 x2' y0' y2' y3' y4 + x0' x3 x4' y0 y1 y2 y4' + x0' x1 x2 x3 x4 y3 y4' + x1 x2' x3 y0 y2 y3 y4' + x1' x3' y0 y1 y2 y3 y4' + x0' x1 x2' y0 y2' y3' y4' + x0 x1' x3' x4' y0 y1 y2' y3  + x0 x1' x2 x3' x4' y0' y1 y3'  + x0 x1' x2 x3' x4' y0 y1' y3' +";
    unsigned c = 0;
    unsigned cpt = 0;
    while (c < s.length()) {
      GRBLinExpr e = 0;
      while (true) {
        while (s[c] == ' ') ++c;
        if (s[c+2] == '\'') {
          if (s[c] == 'x') e += (1 - x[320*r + i + 64*(4 - s[c+1] + '0')]);
          else e += (1 - y[320*r + i + 64*(4 - s[c+1] + '0')]);
          c += 3;
        }
        else {
          if (s[c] == 'x') e += x[320*r + i + 64*(4 - s[c+1] + '0')];
          else e += y[320*r + i + 64*(4 - s[c+1] + '0')];
          c += 2;
        }
        while (s[c] == ' ') ++c;
        if (s[c] == '+') {
          model.addConstr(e >= 1).set(GRB_IntAttr_Lazy, 1);
          c += 1;
          ++cpt;
          break;
        }
      }
    }
    cout << "cpt = " << cpt << endl;
    
  }
}


/* Linear layer modeling */
void ascon_Linear(unsigned r, GRBModel & model, vector<GRBVar> & x, vector<GRBVar> & y, vector<GRBVar> & ca) {

  for (unsigned i = 0; i < 64; ++i) {
    GRBVar u0 = model.addVar(0.0, 2.0, 0.0, GRB_INTEGER);
    u0.set(GRB_IntAttr_BranchPriority, -20);
    GRBVar u1 = model.addVar(0.0, 2.0, 0.0, GRB_INTEGER);
    u1.set(GRB_IntAttr_BranchPriority, -20);
    GRBVar u2 = model.addVar(0.0, 2.0, 0.0, GRB_INTEGER);
    u2.set(GRB_IntAttr_BranchPriority, -20);
    GRBVar u3 = model.addVar(0.0, 2.0, 0.0, GRB_INTEGER);
    u3.set(GRB_IntAttr_BranchPriority, -20);
    GRBVar u4 = model.addVar(0.0, 2.0, 0.0, GRB_INTEGER);
    u4.set(GRB_IntAttr_BranchPriority, -20);

    model.addConstr(y[320*r+i] + y[320*r+(64+i-19) % 64] + y[320*r+(64+i-28) % 64] + x[320*(r+1)+i] == 2*u0).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(y[320*r+64+i] + y[320*r+((64+i-61) % 64)+64] + y[320*r+((64+i-39) % 64)+64] + x[320*(r+1)+64+i] == 2*u1).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(y[320*r+128+i] + y[320*r+((64+i-1) % 64)+128] + y[320*r+((64+i-6) % 64)+128] + x[320*(r+1)+128+i] == 2*u2).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(y[320*r+192+i] + y[320*r+((64+i-10) % 64)+192] + y[320*r+((64+i-17) % 64)+192] + x[320*(r+1)+192+i] == 2*u3).set(GRB_IntAttr_Lazy, 1);
    model.addConstr(y[320*r+256+i] + y[320*r+((64+i-7) % 64)+256] + y[320*r+((64+i-41) % 64)+256] + x[320*(r+1)+256+i] == 2*u4).set(GRB_IntAttr_Lazy, 1);
  }

  for (unsigned l = 0; l < 5; ++l) {
    GRBLinExpr e_in = 0, e_out = 0;
    for (unsigned i = 0; i < 64; ++i) {
      e_in += y[320*r+i];
      e_out += x[320*(r+1) + i];
    }
    model.addConstr(e_in + 10*e_out >= 30);
  }

}


/* Model for the entire Ascon permutation. R is the number of rounds. */
void modelAscon(unsigned R) {

  GRBEnv env = GRBEnv(true);
  env.set("LogFile", "mip1.log");
  env.start();

  GRBModel model = GRBModel(env);

  vector<GRBVar> x (320*R); // set
  vector<GRBVar> y (320*R); // known
  vector<GRBVar> pr0 (64*R); // zero
  vector<GRBVar> pr1 (64*R); // dist
  vector<GRBVar> ca (64*R); // col active

  for (unsigned i = 0; i < 320*R; ++i) x[i] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
  for (unsigned i = 0; i < 320*R; ++i) y[i] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
  for (unsigned i = 0; i < 64*R; ++i) pr0[i] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
  for (unsigned i = 0; i < 64*R; ++i) pr1[i] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
  for (unsigned i = 0; i < 64*R; ++i) ca[i] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);

  for (unsigned i = 0; i < 64*R; ++i) pr0[i].set(GRB_IntAttr_BranchPriority,10);
  for (unsigned i = 0; i < 64*R; ++i) pr1[i].set(GRB_IntAttr_BranchPriority,10);
  for (unsigned i = 0; i < 64*R; ++i) ca[i].set(GRB_IntAttr_BranchPriority,30 + (i%64 == 0 ? 20 : 0));


  if (R > 1) {
    model.addConstr(y[0+320] + y[64+320] + y[128+320] + y[192+320] + y[256+320] >= 1);
    for (unsigned i = 1; i < 64; ++i) {
      model.addConstr(y[0+320] + 2*y[64+320] + 4*y[128+320] + 8*y[192+320] + 16*y[256+320] 
                        >= y[0 + i+320] + 2*y[64 + i+320] + 4*y[128 + i+320] + 8*y[192 + i+320] + 16*y[256 + i+320]);
    }
  }

  GRBLinExpr obj = 0;
  GRBLinExpr obj2 = 0;

  double best_obj = 3*64*R;

  vector<GRBLinExpr> v_obj (R);

  // some known bounds for a given number of rounds, which can help to guide
  // our model.
  unsigned bounds[] = {2,8,40,86,100,129,131,172,186,215,229,258};

  for (unsigned r = 0; r < R; ++r) {
    v_obj[r] = 0;
    if (r == 0) ascon_Sbox_first(model, x, y, ca, pr0, pr1, v_obj[r]);
    else if (r == R-1) ascon_Sbox_last(r, model, x, y, ca, pr0, pr1, v_obj[r]);
    else ascon_Sbox_lazy(r, model, x, y, ca, pr0, pr1, v_obj[r]);
    if (r != R - 1) {
      ascon_Linear(r, model, x, y, ca);
    }
    obj += v_obj[r];
  }

  for (unsigned r1 = 0; r1 < R; ++r1) {
    for (unsigned r2 = r1; r2 < R; ++r2) {
      if (r1 == 0 && r2 == R-1) continue;
      GRBLinExpr e = 0;
      for (unsigned r = r1; r <= r2; ++r) e += v_obj[r];
      model.addConstr(e >= bounds[r2-r1]);
    }
  }

  model.setObjective(obj, GRB_MINIMIZE);

  model.read("tune.prm");

  model.optimize();

   for (unsigned active_lines = R-1; active_lines <= 5*(R-1); ++active_lines) {
    for (unsigned conf = 0; conf < (1u << 5*(R-1)); ++conf) {
      if (__builtin_popcount(conf) != active_lines) continue;
      vector<GRBConstr> myconstr;
      unsigned cpt = 0;
      for (unsigned r = 0; r < R-1; ++r) {
        for (unsigned l = 0; l < 5; ++l) {
          if (((conf >> cpt) & 1) == 0) {
            GRBLinExpr e = 0;
            for (unsigned c = 0; c < 64; ++c) e += y[320*r + 64*l + c];
            myconstr.emplace_back(model.addConstr(e == 0));
          }
          else {
            GRBLinExpr e = 0;
            for (unsigned c = 0; c < 64; ++c) e += y[320*r + 64*l + c];
            myconstr.emplace_back(model.addConstr(e >= 1));
          }
          ++cpt;
        }
      }

      model.optimize();

      if (model.get(GRB_IntAttr_Status) == GRB_OPTIMAL && model.get(GRB_DoubleAttr_ObjVal) < best_obj) {
        cout << model.get(GRB_DoubleAttr_ObjVal) << ": " << active_lines << " - " << conf << endl;
        best_obj = model.get(GRB_DoubleAttr_ObjVal);
        model.addConstr(obj <= best_obj - 1);
        for (unsigned r = 0; r < R-1; ++r) {
          for (unsigned i = 0; i < 320; ++i) {
            if (i%64 == 0) cout << endl;
            cout << (y[320*r + i].get(GRB_DoubleAttr_X) < 0.5 ? 0 : 1);
          }
          cout << endl;

          for (unsigned i = 0; i < 320; ++i) {
            if (i%64 == 0) cout << endl;
            cout << (x[320*(r+1) + i].get(GRB_DoubleAttr_X) < 0.5 ? 0 : 1);
          }
          cout << endl;
        }
        
        //getchar();
      }
      for (auto & c : myconstr) model.remove(c);
    }
  } 

  
  
}


int main(int argc, char const *argv[]) {

  modelAscon(stoi(argv[1]));
  return 0;
  
}


